use super::osmpbfreader;

use osm_helper::LabelConstructor;
use primitives::*;

use std::collections::HashSet;

pub(crate) struct PoiFactory {
    lbl_fac: label::LabelFactory,
}

impl LabelConstructor for PoiFactory {
    fn create_poi(&self, obj: &osmpbfreader::OsmObj) -> Result<PointOfInterest, String> {
        match obj {
            osmpbfreader::OsmObj::Node(node) => self.create_poi_from_node(node),
            osmpbfreader::OsmObj::Way(_way) => {
                return Err("Unsupported object type: Way".to_string())
            }
            osmpbfreader::OsmObj::Relation(_relation) => {
                return Err("Unsupported object type: Relation".to_string())
            }
        }
    }
}

impl PoiFactory {
    pub(crate) fn new(langs: HashSet<String>) -> Self {
        Self {
            lbl_fac: label::LabelFactory::new(langs),
        }
    }

    fn create_poi_from_node(&self, node: &osmpbfreader::Node) -> Result<PointOfInterest, String> {
        self.create_poi_from_tags(
            &node.tags,
            Latitude::new(node.lat()),
            Longitude::new(node.lon()),
            ID::from(node.id),
        )
    }

    fn create_poi_from_tags(
        &self,
        tags: &osmpbfreader::Tags,
        lat: Latitude,
        lon: Longitude,
        id: ID,
    ) -> Result<PointOfInterest, String> {
        let lvl = match level::level_from_tags(tags) {
            Ok(lvl) => lvl,
            Err(msg) => return Err(format!("Level computation failed with error:\n{}", msg)),
        };

        let label = match self.lbl_fac.label_from_tags(&tags) {
            Ok(lbls) => lbls,
            Err(msg) => {
                return Err(format!(
                    "Error: Could not construct label for object {}. Error was \n{}",
                    id.0, msg
                ))
            }
        };

        Ok(PointOfInterest::new(lat, lon, id, lvl, label))
    }
}
