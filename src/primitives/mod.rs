extern crate osmpbfreader;

use self::osmpbfreader::OsmId::{Node, Relation, Way};
use self::osmpbfreader::{NodeId, RelationId, WayId};

pub(crate) mod label;
pub(crate) mod level;

#[derive(Debug, Clone)]
pub struct ID(pub i64);

#[derive(Debug, Clone)]
pub struct Latitude {
    val: f64,
}

#[derive(Debug, Clone)]
pub struct Longitude {
    val: f64,
}

#[derive(Debug)]
pub(crate) struct PointOfInterest {
    lat: Latitude,
    lon: Longitude,
    uid: ID,
    level: level::Level,
    label: label::Label,
}

// Implement Latitude
impl Latitude {
    pub fn new(val: f64) -> Self {
        assert!(-90. <= val && val <= 90.);
        Latitude { val }
    }
}

// Implement Longitude
impl Longitude {
    pub fn new(val: f64) -> Self {
        assert!(-180. <= val && val <= 180.);
        Longitude { val }
    }
}

// Implement ID
impl From<osmpbfreader::NodeId> for ID {
    fn from(id: osmpbfreader::NodeId) -> Self {
        let NodeId(id_val) = id;

        ID(id_val)
    }
}
impl From<osmpbfreader::OsmId> for ID {
    fn from(id: osmpbfreader::OsmId) -> Self {
        let id_val = match id {
            Node(NodeId(id_1)) => id_1,
            Relation(RelationId(id_2)) => id_2,
            Way(WayId(id_3)) => id_3,
        };
        ID(id_val)
    }
}

// Implement PointOfInterest
impl PointOfInterest {
    pub fn new(
        lat: Latitude,
        lon: Longitude,
        uid: ID,
        level: level::Level,
        label: label::Label,
    ) -> Self {
        Self {
            lat,
            lon,
            uid,
            level,
            label,
        }
    }

    pub fn get_id(&self) -> ID {
        self.uid.clone()
    }

    pub fn get_label(&self) -> &label::Label {
        &self.label
    }

    pub fn get_lat(&self) -> Latitude {
        self.lat.clone()
    }

    pub fn get_lon(&self) -> Longitude {
        self.lon.clone()
    }
}
