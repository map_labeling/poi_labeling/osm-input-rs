use osmpbfreader::Tags;

use std::collections::{HashMap, HashSet};

#[derive(Debug)]
pub struct Label {
    label: String,
    loc_label: HashMap<String, String>,
}

pub struct LabelFactory {
    languages: HashSet<String>,
}

// implement Label
impl Label {
    pub fn new(label: String) -> Self {
        Self {
            label: label,
            loc_label: HashMap::new(),
        }
    }

    pub fn add(&mut self, language: String, label: String) {
        self.loc_label.insert(language, label);
    }

    pub fn get_label(&self) -> String {
        self.label.to_string()
    }

    pub fn get_localized_label(&self, lang: &String) -> Result<&String, String> {
        match self.loc_label.get(lang) {
            Some(label) => return Ok(label),
            None => {
                return Err(format!(
                    "Could not find label corresponding to language {}",
                    lang
                ))
            }
        }
    }
}

// implement LabelFactory
impl LabelFactory {
    pub fn new(langs: HashSet<String>) -> Self {
        Self { languages: langs }
    }

    pub(crate) fn label_from_tags(&self, tags: &Tags) -> Result<Label, String> {
        let name = match tags.get("name") {
            Some(name) => name.to_string(),
            None => return Err("Tag set does not contain name information.".to_string()),
        };

        let mut lbl = Label::new(name);

        for (k, v) in tags.iter() {
            if !k.starts_with("name:") {
                continue;
            }

            let lang = k.trim_left_matches("name:").to_string();

            if self.languages.contains(&lang) {
                lbl.add(lang, v.to_string());
            }
        }

        Ok(lbl)
    }
}
