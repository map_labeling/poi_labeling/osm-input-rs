use super::osmpbfreader;

use std::fs::File;
use std::path::Path;

use super::primitives;

pub(crate) trait LabelConstructor {
    fn create_poi(&self, &osmpbfreader::OsmObj) -> Result<primitives::PointOfInterest, String>;
}

pub(crate) fn import_poi_data<T: LabelConstructor>(
    path: &Path,
    creator: &T,
) -> Vec<primitives::PointOfInterest> {
    let r = File::open(path).expect(&format!("Error opening file {}", path.display()));

    let mut pbf = osmpbfreader::OsmPbfReader::new(r);
    let mut pois = Vec::new();

    for obj in pbf.par_iter().map(Result::unwrap) {
        let poi = match creator.create_poi(&obj) {
            Ok(poi) => poi,
            Err(_) => continue,
        };

        pois.push(poi);
    }

    pois
}
