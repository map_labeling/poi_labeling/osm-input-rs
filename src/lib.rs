extern crate osmpbfreader;

mod font_helper;

mod poi_helper;

mod osm_helper;

mod primitives;

use std::collections::HashSet;
use std::path::Path;

pub fn test() {
    println!("Hello from the library!");

    let mut langs = HashSet::new();
    langs.insert("de".to_string());
    let poi_fac = poi_helper::PoiFactory::new(langs);

    let path = Path::new("bremen-latest.osm.pbf");
    let pois = osm_helper::import_poi_data(path, &poi_fac);

    println!("Imported {} data items", pois.len());

    for p in pois {
        println!(
            "Poi {:?}: at position ({:?}, {:?}) with label {}",
            p.get_id(),
            p.get_lat(),
            p.get_lon(),
            p.get_label().get_label()
        );
    }
}

#[cfg(test)]
mod tests {
    use osm_helper;
    use poi_helper;

    use std::collections::HashSet;
    use std::path::Path;

    #[test]
    fn it_works() {
        let mut langs = HashSet::new();
        langs.insert("de".to_string());
        let poi_fac = poi_helper::PoiFactory::new(langs);

        let path = Path::new("bremen-latest.osm.pbf");
        let data = osm_helper::import_poi_data(path, &poi_fac);

        println!("Imported {} data items", data.len());
    }
}
